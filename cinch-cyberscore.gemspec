# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require 'cinch/plugins/cyberscore/version'

Gem::Specification.new do |gem|
  gem.name          = "cinch-cyberscore"
  gem.version       = Cinch::Plugins::Cyberscore::VERSION
  gem.authors       = ["Ricardo Mendes"]
  gem.email         = ["rokusu@gmail.com"]
  gem.description   = %q{Cyberscore plugin for Cinch}
  gem.summary       = %q{Cyberscore plugin for Cinch}
  gem.homepage      = "http://github.com/cyberscore/cinch-cyberscore"

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.add_development_dependency 'rake'
  
  gem.add_dependency 'cinch'
  gem.add_dependency 'hyperscore', '>= 0.0.12'
end
