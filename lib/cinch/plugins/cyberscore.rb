require 'cinch'
require 'hyperscore'

module Cinch
  module Plugins

    class Cyberscore
      include Cinch::Plugin


      self.prefix = lambda{ |m| Regexp.new("^" + Regexp.escape(m.bot.nick + ": " ))}


      def initialize(*args)
        super
        @client = Hyperscore::Client.new
      end


      match "help", method: :help
      def help(m)
        m.user.notice %q{Usage:
      JezeBot: command subcommand
    Available commands:
      help
      profile <username>
      dashboard <username>
      news
      news last
      subs
      subs last
      games
      games last}
      end

      match "news last", method: :news_last
      def news_last(m)
        it = @client.news.first

        m.reply "#{it.date}: #{it.headline} (#{it.site})"
      end

      match "news", method: :news
      def news(m)
        @client.news.first(5).each do |it|
          m.user.notice "#{it.date}: #{it.headline} (#{it.site})"
        end
      end

      match "subs last", method: :subs_last
      def subs_last(m)
        it = @client.submissions.first

        m.reply "#{it.user_name} got #{it.position} place on '#{it.game_name} - #{it.chart_name}' with #{it.record} (#{it._links['self']['href']})"
      end

      match "subs", method: :subs
      def subs(m)
        @client.submissions.each do |it|
          m.user.notice "#{it.user_name} | #{it.game_name} - #{it.chart_name} | #{it.position} | #{it.record} | (#{it._links['self']['href']})"
        end
      end

      match "games last", method: :games_last
      def games_last(m)
        it = @client.games.first

        m.reply "#{it['id']}. #{it['name']} (#{it._links['self']['href']})"
      end

      match "games", method: :games
      def games(m)
        @client.games.each do |it|
          m.user.notice "#{it.attributes['id']}. #{it.attributes['name']} (#{it.url})"
        end
      end


      match "profile",       method: :profile
      match /profile (.+)/i, method: :profile
      def profile(m, target=nil)
        target ||= m.user.nick

        m.reply "#{m.user.nick}: #{target} @ #{ @client.profile target }"
      end

      match 'dashboard',       method: :dashboard
      match /dashboard (.+)/i, method: :dashboard
      def dashboard(m, target=nil)
        target ||= m.user.nick

        m.reply "#{m.user.nick}: #{target} @ #{ @client.dashboard target }"
      end


      match "records",      method: :records
      match /records (.+)/, method: :records
      def records(m, target=nil)
        result = user_exists(m, target || m.user.nick)

        result and m.reply "#{m.user.nick}: #{result['username']} @ #{ result['_links']['records']['href'] }"
      end

    end

  end
end